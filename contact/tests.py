from django.test import TestCase, Client
from .models import Feedback
from .views import *
from .urls import *


class FeedbackUnitTest(TestCase):
    def test_Feedback_exists(self):
        Feedback.objects.create(feedback="test")
        howmany = Feedback.objects.all().count()
        self.assertEqual(howmany,1)

class TemplateAccess(TestCase):
    def test_Contact_accessed(self):
        url = Client().get('/contact/')
        self.assertTemplateUsed(url,'contact.html')
    
