from django.shortcuts import render, redirect
from .models import Feedback
from . import forms

# Create your views here.
def contact(request):
    if request.method == 'POST':
        form = forms.FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('contact:contact')

    else:
        form = forms.FeedbackForm()
    return render(request, 'contact.html', {'form': form, "list_feedback": Feedback.objects.all()})