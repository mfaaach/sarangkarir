from django.conf.urls import url
from django.urls import path
from .views import articles, articles_create

app_name = 'articles'

urlpatterns = [
    
    path('articles/', articles, name='articles'),
    path('articles_create/', articles_create, name='article2')
]