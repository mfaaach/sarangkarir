from django import forms
from .models import Articles


class ArticleForm(forms.ModelForm):

    title = forms.CharField(widget=forms.TextInput(attrs={
            "class" : "form-control",
            "required" : True,
            "placeholder":"Input your title...",
            }), label ="Title")

    paragraph = forms.CharField(widget=forms.Textarea(attrs={
            'cols' : 100,
            "class" : "form-control",
            "required" : True,
            "placeholder":"Input your paragraphs...",
            }), label ="Paragraphs")

    class Meta:
        model = Articles
        fields = [
         'title',
         'paragraph'
         ]

           
           
           
           
