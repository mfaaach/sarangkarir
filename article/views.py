from django.shortcuts import render, redirect
from .models import Articles
from . import forms

# Create your views here.

def articles(request):
    articles = Articles.objects.all()
    return render(request, 'article.html', {'article': articles})

def articles_create(request):
    if request.method == 'POST':
        form = forms.ArticleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('article:articles')
            

    else:
        form = forms.ArticleForm()
        return render(request, 'article2.html', {'form': form})

def view_more(request):
    return render(request, '../../homepage/templates/index.html')
