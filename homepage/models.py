from django.db import models

# Create your models here.
INDUSTRY_CHOICES = (
    (1,"Technology"),
    (2,"Healthcare"),
)

BACKGROUND_CHOICES = (
    (1,"University"),
    (2,"High School"),
)

SKILL_CHOICES = (
    (1,"Managing"),
    (2,"Working"),
)

WAGE_CHOICES = (
    (1,"Rp 1mi - Rp 15mi"),
    (2,"Rp 15mi - Rp 30mi"),
)   

class Job(models.Model):
    name = models.CharField(max_length = 100, default='Nama')
    description = models.CharField(max_length = 500, default = 'Deskripsi')
    foto = models.CharField(max_length = 500, default = 'url foto')
    industry = models.IntegerField(default=1, choices=INDUSTRY_CHOICES)
    background = models.IntegerField(default=1, choices=BACKGROUND_CHOICES)
    skill = models.IntegerField(default=1, choices=SKILL_CHOICES)
    wage = models.IntegerField(default=1, choices=WAGE_CHOICES)

    def __str__(self):
        return self.name + " - " + self.getIndustry() + " - " + self.getBackground() + " - " + self.getSkill() + " - " + self.getWage()

    def getIndustry(self):
        if self.industry == 1:
            return "Technology"
        elif self.industry == 2:
            return "Healthcare"
    
    def getBackground(self):
        if self.background == 1:
            return "University"
        elif self.background == 2:
            return "High School"

    def getSkill(self):
        if self.skill == 1:
            return "Managing"
        elif self.skill == 2:
            return "Working"

    def getWage(self):
        if self.wage == 1:
            return "Rp 1mi - Rp 15mi"
        elif self.wage == 2:
            return "Rp 15mi - Rp 30mi"

class User(models.Model):
    name = models.CharField(max_length = 100, default='Nama')

    def __str__(self):
        return self.name