from django import forms

INDUSTRY_CHOICES = (
    (1,"Technology"),
    (2,"Healthcare"),
)

BACKGROUND_CHOICES = (
    (1,"University"),
    (2,"High School"),
)

SKILL_CHOICES = (
    (1,"Managing"),
    (2,"Working"),
)

WAGE_CHOICES = (
    (1,"Rp 1mi - Rp 15mi"),
    (2,"Rp 15mi - Rp 30mi"),
)

class EduForm(forms.Form):
    name = forms.CharField(max_length=100, label="Your Name", required=True)
    name.widget.attrs['class'] = 'form-control'
    name.widget.attrs['style'] = 'width: 50vh;'

    industry = forms.ChoiceField(choices = INDUSTRY_CHOICES, label="Choose an Industry", initial='', required=True,
    )
    industry.widget.attrs['class'] = 'form-control'
    industry.widget.attrs['style'] = 'width:50vh;'
    
    background = forms.ChoiceField(choices = BACKGROUND_CHOICES, label="Educational Background", initial='', required=True,
    )
    background.widget.attrs['class'] = 'form-control'
    background.widget.attrs['style'] = 'width:50vh;'

    skill = forms.ChoiceField(choices = SKILL_CHOICES, label="Choose Your Skill", initial='', required=True,
    )
    skill.widget.attrs['class'] = 'form-control'
    skill.widget.attrs['style'] = 'width:50vh;'

    wage = forms.ChoiceField(choices = WAGE_CHOICES, label="Desired Wage", initial='', required=True,
    )
    wage.widget.attrs['class'] = 'form-control'
    wage.widget.attrs['style'] = 'width:50vh;'

    # def __init__(self, *args, **kwargs):
    #     super(EduForm, self).__init__(*args, **kwargs)
    #     self.fields['status'].widget.attrs['class'] = 'form-control'
    # class Meta:
    #     model = Jadwal
    #     fields = [
    #         'nama_kegiatan',
    #         'kategori',
    #         'tempat',
    #         'tanggal',
    #         'waktu',
    #     ]