from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *
from .models import *

class Homepage(TestCase):
    def test_preference_url(self):
        response = Client().get('/preference/')
        self.assertEqual(response.status_code, 200)

    def test_preference_using_preference_template(self):
        response = Client().get('/preference/')
        self.assertTemplateUsed(response, 'preference.html')

    def test_index_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')


class ModelTest(TestCase):
    def test_compare_input_user(self):
        userNew = Job.objects.create(
            name="pow", 
            description="keren", 
            foto="url", 
            industry = 1,
            background = 1,
            skill = 1,
            wage = 2,
            )
        user = Job.objects.filter(
            industry = 1,
            background = 1,
            skill = 1,
            wage = 2,
            ).first()
        self.assertEqual(user.name, "pow")

    def test_model_can_create_new_user(self):
        userNew = User.objects.create(name="pow")
        user = User.objects.all().count()
        self.assertEqual(user, 1)

