from django.shortcuts import render
from .forms import eventForm
from .models import event as jobfair
from django.shortcuts import redirect
import datetime

def sched_add(request):
    if request.method == 'POST':
        form = eventForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('event:event')
    else:
        form = eventForm()

    content = {'title' : 'Form event',
                'form' : form}

    return render(request, 'sched_add.html', content)

def event(request):
    data = jobfair.objects.all()
    context = {
        'data' : data,
    }
    return render(request, 'event.html', context)

def sched_edit(request, pk):
    post = jobfair.objects.get(pk=pk)
    if request.method == "POST":
        form = eventForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('event:event')
    else:
        form = eventForm(instance=post)

    content = {'title' : 'Form event',
                'form' : form,
                'obj' : post}
    return render(request, 'sched_edit.html', content)

def sched_delete(request, pk):
    jobfair.objects.filter(pk=pk).delete()
    data = jobfair.objects.all()
    return redirect('event:event')

