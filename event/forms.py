from django import forms
from .models import event
from django.forms import ModelForm

class eventForm(ModelForm):
    class Meta:
        model= event
        fields = ['dates', 'time', 'eventname', 'location', 'rsvp', 'eventdesc']
        labels = {
            'dates' : 'Date', 'time' : 'Time', 'eventname' : 'Event Name', 'location' : 'Location', 'rsvp' : 'RSVP', 'eventdesc' : 'Description'
        }
        widgets = {
            'dates' : forms.DateInput(attrs={'class': 'form-control',
                                        'type' : 'date'}),
            'time' : forms.TimeInput(attrs={'class': 'form-control',
                                        'type' : 'time'}),
            'eventname' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Name of the event'}),
            'location' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Location'}),
            'rsvp' : forms.TextInput(attrs={'class': 'form-control',
                                              'type' : 'text'}),
            'eventdesc' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Description of the event'}),
        }
